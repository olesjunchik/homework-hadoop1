## Task description

**Buisness logic:**
	For dataset http://data.computational-advertising.org calculate amount of high-bid-priced (more than 250) impression events by city, where each city presented with its name rather than id. Additional info about contest: http://contest.ipinyou.com/ipinyou-dataset.pdf. (Use only imp.201310(19-27).txt.bz2 files as a main dataset)

**Output format:**
	SequenceFile with Snappy encoding (Read content of compressed file from console using
command line)

**Additional requirements:**
	Custom Partitioner is used (by OperationSystemType)
	
**Report includes:**
	Screenshot of successfully uploaded file into HDFS
	
---

## Requarements

- java (version 6 or later)
- maven
- docker
- git

---

## Preparation and build

Firstly build docker image with hadoop

```
sudo ./hadoop-docker/build-image.sh
```

Start docker container with hadoop in another console

```
sudo ./hadoop-docker/start-container.sh
```

After that run preparation script. It compile java code, copy data jar files into container.

```
./prepare.sh
```

Check to hadoop console and execute run script. It start hadoop, execute test word-count application and after that load data into hdfs, start high-bid-price events counter program and print the answer.

```
./run.sh
```