#!/bin/bash
mvn package
MASTERID=$(sudo docker ps | grep hadoop-master | cut -d " " -f 1)
sudo docker cp ./target/high-bid-priced-events-count-1.0.jar $MASTERID:/root
sudo docker cp data $MASTERID:/root
sudo docker cp run.sh $MASTERID:/root
