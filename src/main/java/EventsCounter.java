import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.io.BufferedReader;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.compress.SnappyCodec;

/** Main class of the hadoop application
 * Calculates amount of high-bid-priced (more then 250) events by city
 */
public class EventsCounter {
    /** Mapper class
     * Makes pair (City, 1) for high-bid-priced events from each line
     */
    public static class EventsMapper extends Mapper<Object, Text, Text, IntWritable>
    {
    	//Map with the conformation of cityID and it name
    	private Map<String, String> cities = new HashMap<String, String>();
        /**
         * Map method
         * key and context are unused params
         * value containing a text line for mapping
         */
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            //Splitting line into set of fields
            String[] fields = value.toString().split("\t");
            //Checking Bidding Price value, it should be more then 250
            int bidprice = Integer.parseInt(fields[19]);
            if (bidprice > 250) {
                //Getting city by it ID
                String cityID = fields[7];
                String city = cities.get(cityID);
                //If city hasn't found use it ID
                city = (city == null) ? cityID : city;
                //Creating pair (CityID, 1)
                context.write(new Text(city), new IntWritable(1));
            }
        }
        
        protected void setup(Context context) throws IOException, InterruptedException {
        	//Getting cache path
		    Path[] distributedCacheFiles = DistributedCache.getLocalCacheFiles(context.getConfiguration());
		    for (Path distributedCacheFile : distributedCacheFiles) {
		    	//Reading cache to buffer
				BufferedReader bufferedReader = new BufferedReader(new java.io.FileReader(distributedCacheFile.toString()));
				//Getting lines from buffer with cityID and name
				String line;
				String[] fields;
				for(int i = 0; (line = bufferedReader.readLine()) != null; i++) {
				    if(i == 0)
				        fields = line.split(" ");
				    else
				        fields = line.split("\t");
					//Putting into map pairs (cityID, name)
				    cities.put(fields[0], fields[1]);
				}
		    }
		}
    }

    /** Reducer class
     * Counting sum of high-bid-priced events for each city
     */
    public static class EventsReducer extends Reducer<Text,IntWritable,Text,IntWritable>
    {
        /**
         * Reduce method
         * key containing city name
         * value
         * context are unused param
         */
        public void reduce(Text key, Iterable<IntWritable> value, Context context) throws IOException, InterruptedException {
            //Counting summary of high-bid-events for each city
            int sum = 0;
            for(IntWritable x: value) {
		        sum += x.get();
		    }
		    context.write(key, new IntWritable(sum));
        }
    }
    
    /** Partitioner class
     */
    public static class EventsPartitioner extends Partitioner<Text, IntWritable> {
		public int getPartition(Text key, IntWritable value, int numReduceTasks) {
		    if(numReduceTasks == 0)
		    	return 0;
		    return ((key.hashCode() & Integer.MAX_VALUE) % numReduceTasks);
		}
	}
        
    /** MapReduce job entry point
     */
    public static void main(String[] args) throws Exception
    {
        //Creating new configuration
        Configuration conf = new Configuration();
        //Creating a job named "High-bid-priced events counter"
        Job job = Job.getInstance(conf, "High-bid-priced events counter");
        //Setting number of reducers thats provided as forth argument
        job.setNumReduceTasks(Integer.parseInt(args[3]));
        //Setting main MapReduce class to EventsCounter.class
        job.setJarByClass(EventsCounter.class);
        
        //Setting mapper class to EventsMapper.class
        job.setMapperClass(EventsMapper.class);
        //Setting partitioner class to EventsPartitioner.class
        job.setPartitionerClass(EventsPartitioner.class);
        //Setting reducer class to EventsReducer.class
        job.setReducerClass(EventsReducer.class);
        
        //Setting output key class to text
        job.setOutputKeyClass(Text.class);
        //Setting output value class to IntWritable, this value containing sum of events
        job.setOutputValueClass(IntWritable.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        //Setting input and output paths to paths provided in command line arguments
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        //Setting snappy encoding output
        FileOutputFormat.setCompressOutput(job, true);
    	FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
		SequenceFileOutputFormat.setOutputCompressionType(job,CompressionType.BLOCK);
        
        // Adding the cache file URL which we passed as the third argument
        DistributedCache.addCacheFile(new Path(args[2]).toUri(), job.getConfiguration());
        
        //Waiting for the job completion
        if(job.waitForCompletion(true))
            System.exit(0);
        //If job failed
        System.exit(1);
    }
}
