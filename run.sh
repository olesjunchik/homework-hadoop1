#!/bin/bash

./start-hadoop.sh
./run-wordcount.sh

hdfs dfs -put data/city.en.txt city.en
hdfs dfs -put data data
hdfs dfs -rm data/city.en.txt
hdfs dfs -ls
hdfs dfs -rm -f -r output
hadoop jar high-bid-priced-events-count-1.0.jar EventsCounter data output city.en 2
hdfs dfs -text output/part-r-00000
